#ifndef __H_MEMORY_MANAGER__
#define __H_MEMORY_MANAGER__
#define MEMORY MemoryManager::Get()
#include <vector>

#include "memory_block.h"
#include "doube_link_list.h"
#include <iostream>
#include <cassert>

const int NUM_MEMORY_BLOCKS = 2048 ;
const int MEMORY_SIZE = NUM_MEMORY_BLOCKS * NUM_MEMORY_BLOCKS * 16;

class MemoryManager{
  ///Constructor
  MemoryManager();
  ///Destructor
  ~MemoryManager();
  ///Get new block
  MemoryBlock * GetNewBlock();
  static MemoryManager *ptr_;
  //True if two blocks are continues
  bool AreBlockContinuous(const MemoryBlock *first, const MemoryBlock *second);

  std::vector<MemoryBlock> blocks_array_[NUM_MEMORY_BLOCKS];
  DoubleLinkList free_blocks_;
  DoubleLinkList used_blocks_;
  DoubleLinkList unused_blocks_;
  std::vector<void *>big_memory_block;
  int managed_size_;
  int allocated_size_;
  //uint32 count blocks
  int count_blocks = 0;
public:
  void Init();
  void Resize();
    static MemoryManager * Get();
  ///Get block of memory
  void* GetBlock(int size_block);
  ///Free block of memory
  void FreeBlock(void *ptr);
  ///Get total memory managed
  int GetManagedMemorySize()const;
  ///Get totl allocated memory
  int GetAllocatedMemorySize()const;
  //Check coherence
  void CheckCoherence()const;

};

inline bool MemoryManager::AreBlockContinuous(const MemoryBlock *first, const MemoryBlock *second){
  return (int)first->GetPtr() + first->GetSize() == (int)second->GetPtr();
}
inline MemoryBlock* MemoryManager::GetNewBlock(){
  MemoryBlock * new_block = (MemoryBlock *)unused_blocks_.PopFront();
  if (!new_block){
    Resize();
    new_block = (MemoryBlock*)unused_blocks_.PopFront();

     assert(new_block&&"No memory blocks left");


  }
  return new_block;
}

#endif
