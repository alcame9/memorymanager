#ifndef __H_DOUBLE_LINK_LIST__
#define __H_DOUBLE_LINK_LIST__
#include "double_link_element.h"
#include "double_link_iterator.h"

class DoubleLinkList{
public:

  DoubleLinkList();
  ~DoubleLinkList();
    //Functions
    ///Push element to end
  void PushBack(DoubleLinkElement * element);
    ///Push elemtn to the top of list
  void PushFront(DoubleLinkElement * element);
  ///Pop element to end
  DoubleLinkElement* PopBack();
  ///Pop elemtn to the top of list
  DoubleLinkElement* PopFront();
  //Void remove element
  void Remove(DoubleLinkElement * element);
  ///Void remove element byh index
  void Remove(int index);
  ///Inster element into index
  void Inster(DoubleLinkElement * element, int index);
  ///Insert element afther other element
  void InsterAfter(DoubleLinkElement * element, DoubleLinkElement *after_element);
  ///Inser before other element
  void InsterBefore(DoubleLinkElement * element, DoubleLinkElement *before_element);
  ///Replace one element by another
  void Replace(DoubleLinkElement * element, DoubleLinkElement * replace);
  ///Return size of list
  int Size();
  ///Overload operator
   DoubleLinkElement& operator [](size_t idx);
   DoubleLinkElement* Get(int index);
   ///Get index of an element
   int GetElementIndex(const DoubleLinkElement *element)const;
   //Return true if element is on the list, otherwise return false
   bool Exist(const DoubleLinkElement * element)const;
   ///Get iteratoe
   DoubleLinkIterator GetIterator()const;
   //Clear list
   void Clear();
protected:
  DoubleLinkElement *first_;
  DoubleLinkElement *last_;
  DoubleLinkElement  first_element_;
  DoubleLinkElement last_element_;
  int size_;
};

///INLINE FUNCTIONS


inline void DoubleLinkList::PushBack(DoubleLinkElement * element){
  InsterAfter(element, last_->previus_);
}

inline void DoubleLinkList::PushFront(DoubleLinkElement * element){
  InsterAfter(element, first_);
}


inline void DoubleLinkList::Remove(int index){
  Remove(Get(index));
}

inline int DoubleLinkList::Size(){
  return size_;
}


inline bool DoubleLinkList::Exist(const DoubleLinkElement * element)const{
  return (GetElementIndex(element) != -1);
}

inline DoubleLinkIterator DoubleLinkList::GetIterator()const{
  return DoubleLinkIterator(first_, last_);
}

#endif
