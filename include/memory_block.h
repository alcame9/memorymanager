/**

This class represents a memomry block
*/
#ifndef __H_MEMORY_BLOCK__
#define __H_MEMORY_BLOCK__
#include "double_link_element.h"

class MemoryBlock : public DoubleLinkElement{
  unsigned char *ptr_;
  int size_;
public:
  MemoryBlock();
  ///Initialize memory block whit pointer and size
  void Init(unsigned char *ptr, int size);
  ///Get block pointer
  unsigned char* GetPtr()const;
  //Get size of the block
  int GetSize()const;
  ///Resize the block
  void Resize(int size);
  ///Clear block
  void Clear();


};
////INLINE FUNCTIONS
inline MemoryBlock::MemoryBlock(){
  Init(nullptr, 0);
}

inline void MemoryBlock::Init(unsigned char *ptr, int size){
  ptr_ = ptr;
  size_ = size;

}

inline unsigned char* MemoryBlock::GetPtr()const{
  return ptr_;
}

inline int MemoryBlock::GetSize()const{
  return size_;
}

inline void MemoryBlock::Resize(int size){
  size_ = size;

}

inline void MemoryBlock::Clear(){
  Init(nullptr, 0);
}

#endif
