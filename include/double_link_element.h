#ifndef __H_DOUBLE_LINK_ELEMENT__
#define __H_DOUBLE_LINK_ELEMENT__

class DoubleLinkElement{

public:
  void Unlink();
  DoubleLinkElement *previus_;
  DoubleLinkElement *next_;
protected:
  DoubleLinkElement();
  DoubleLinkElement(const DoubleLinkElement &other);
  ~DoubleLinkElement();
  friend class DoubleLinkIterator;
  friend class DoubleLinkList;
};

///Inline functions

inline DoubleLinkElement::DoubleLinkElement(){
 Unlink();
}

inline DoubleLinkElement::DoubleLinkElement(const DoubleLinkElement & other){
  previus_ = other.previus_;
  next_ = other.next_;
}

inline DoubleLinkElement::~DoubleLinkElement(){

}

inline void DoubleLinkElement::Unlink(){
  previus_ = nullptr;
  next_ = nullptr;
}

#endif
