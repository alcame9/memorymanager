#ifndef __H_DOUBLE_LINK_ITERATOR__
#define __H_DOUBLE_LINK_ITERATOR__
#include <iostream>
#include "double_link_element.h"
#include <cassert>

class DoubleLinkIterator{
  DoubleLinkElement * current_element_;
  const DoubleLinkElement *first_element_;
  const DoubleLinkElement *last_element_;

public:
  DoubleLinkIterator(const DoubleLinkElement * first, const DoubleLinkElement * last);
  DoubleLinkIterator(const DoubleLinkIterator& other);
  DoubleLinkIterator();
  ~DoubleLinkIterator();
  /**
    @brief Get pointing element
  */
  DoubleLinkElement * GetElement()const;
  ///Check if its the first element
  bool IsFirst();
  ///Check if its last element
  bool IsLast();
  //Set position depending element
  void SetPositionByElement(DoubleLinkElement *element);
  //Reset iterator to first position
  void Reset();
  ///Reset iterator to last position
  void ResetLast();
  ///Overload operator ++
  void operator ++();
///Overload operator --
  void operator --();

};

///Inline functions


inline DoubleLinkIterator::DoubleLinkIterator(const DoubleLinkElement * first, const DoubleLinkElement * last){
  first_element_ = first;
  last_element_ = last;
  Reset();
}
inline DoubleLinkIterator::DoubleLinkIterator(const DoubleLinkIterator& other){
  first_element_ = other.first_element_;
  last_element_ = other.last_element_;
  current_element_ = other.current_element_;
}
inline DoubleLinkIterator::DoubleLinkIterator(){

}
inline DoubleLinkIterator::~DoubleLinkIterator(){

}

inline DoubleLinkElement *  DoubleLinkIterator::GetElement()const{
  return current_element_;
}

inline bool DoubleLinkIterator::IsFirst(){
  return current_element_ == first_element_;
}

inline bool DoubleLinkIterator::IsLast(){
  return current_element_ == last_element_;
}

inline void DoubleLinkIterator::SetPositionByElement(DoubleLinkElement *element){

    current_element_ = element;
}

inline void DoubleLinkIterator::Reset(){
  current_element_ = first_element_->next_;
}

inline void DoubleLinkIterator::ResetLast(){
  current_element_ = last_element_->previus_;
}

inline void DoubleLinkIterator::operator ++(){
  assert(!IsLast() && "Currentt last element.Can't increment." );
  current_element_ = current_element_->next_;
}

inline void DoubleLinkIterator::operator --(){
   assert(!IsFirst() && "Current initial element. Can't decrement.");
  current_element_ = current_element_->previus_;
}

#endif
