#include <iostream>
#include "memory_manager.h"
#include <string>
#include <iostream>
using namespace std;

struct Data {
	string name;
	float f ;
	int i;
	
	void* operator new(size_t size){
		return MEMORY->GetBlock(size);
	}
	void  operator delete(void * ptr) {
		MEMORY->FreeBlock(ptr);
	}

};

struct Data4: public Data {
	int e;
	double pp;
	void* operator new(size_t size) {
		return MEMORY->GetBlock(size);
	}
	void  operator delete(void * ptr) {
		MEMORY->FreeBlock(ptr);
	}
};

int main(int argc, char* argv[]) {
	int i = 0;
	MEMORY->Init();


	Data *data;

	Data4 *data4 = new Data4();
	data4->name = "cat";
	while (i < 1000){
		data = new Data();
		data->name = "dog";
		data->f = i;
		data4->pp = 1.0f;
		std::cout << data->name << "  " << data->f << std::endl;
		std::cout << data4->name << "  " << data4->pp << std::endl;
		delete data;
		i++;
	}

	delete data4;
	return 0;
}