#include "memory_manager.h"
#include "memory_block.h"

#include <iostream>
MemoryManager* MemoryManager::ptr_ = nullptr;
MemoryManager::MemoryManager(){

}
MemoryManager::~MemoryManager(){
  for (int i = 0; i < count_blocks; i++){
    free(big_memory_block[i]);
  }

}

void MemoryManager::Init(){

  allocated_size_ = 0;
  managed_size_ = MEMORY_SIZE;
  big_memory_block.push_back(malloc(managed_size_));
  for (int i = 0; i < NUM_MEMORY_BLOCKS; i++){
    blocks_array_[0].push_back(MemoryBlock());
  }
  MemoryBlock *first_block = &blocks_array_[0][0];
  first_block->Init((unsigned char*)big_memory_block[0], managed_size_);
  free_blocks_.PushBack((DoubleLinkElement*)first_block);
  count_blocks++;
  for (int i = 1; i < NUM_MEMORY_BLOCKS; ++i){
    unused_blocks_.PushBack((DoubleLinkElement*)&blocks_array_[0][i]);
  }
}

void MemoryManager::Resize(){
  big_memory_block.push_back(malloc(MEMORY_SIZE));
  for (int i = 0; i < NUM_MEMORY_BLOCKS; i++){
    blocks_array_[count_blocks].push_back(MemoryBlock());
  }
  MemoryBlock *first_block = &blocks_array_[count_blocks][0];
  first_block->Init((unsigned char*)big_memory_block[count_blocks], managed_size_);
  free_blocks_.PushBack((DoubleLinkElement*)first_block);
  for (int i = 1; i < NUM_MEMORY_BLOCKS; i++){
    unused_blocks_.PushBack((DoubleLinkElement*)&blocks_array_[count_blocks][i]);
  }
  count_blocks++;
}
void* MemoryManager::GetBlock(int size_block){
  MemoryBlock * selected_free_block_ = nullptr;
  MemoryBlock *allocated_block_ = nullptr;

  DoubleLinkIterator free_iterator = free_blocks_.GetIterator();
  bool found = false;
  while (!found && !free_iterator.IsLast()){
    selected_free_block_ = (MemoryBlock*)free_iterator.GetElement();
    found = selected_free_block_->GetSize() >= size_block;
    ++free_iterator;
  }

  if (!found){
    // resize();
    // get_block(size_block);
    assert(found && "Not block found");
    return nullptr;
  }
  //update free and used list
  if (size_block == selected_free_block_->GetSize()){
    free_blocks_.Remove((DoubleLinkElement*)selected_free_block_);
    used_blocks_.PushBack((DoubleLinkElement*)selected_free_block_);
    allocated_block_ = selected_free_block_;
  }
  else{
    //update free block
    allocated_block_ = GetNewBlock();
    allocated_block_->Init(selected_free_block_->GetPtr(), size_block);
    used_blocks_.PushBack((DoubleLinkElement*)allocated_block_);
    //Add used block
	unsigned char* moved_ptr = selected_free_block_->GetPtr() + size_block;
	int resized_size = selected_free_block_->GetSize() - size_block;
    selected_free_block_->Init(moved_ptr, resized_size);
  }
  allocated_size_ += size_block;
  return allocated_block_->GetPtr();
}
///Free block of memory
void MemoryManager::FreeBlock(void *ptr){
  DoubleLinkIterator used_iterator = used_blocks_.GetIterator();
  MemoryBlock * memory_block = nullptr;
  unsigned char* uint_ptr = (unsigned char*)ptr;
  bool found = false;

  while (!found && !used_iterator.IsLast()) {
    memory_block = (MemoryBlock*)used_iterator.GetElement();
    found = memory_block->GetPtr() == ptr;
    ++used_iterator;
  }

  assert(found && "Elemet not managed cant be deleted");


  allocated_size_ -= memory_block->GetSize();
  //Remove from used
  used_blocks_.Remove((DoubleLinkElement*)memory_block);
  DoubleLinkIterator free_bloc_it = free_blocks_.GetIterator();
  while (!free_bloc_it.IsLast() && uint_ptr > ((MemoryBlock*)free_bloc_it.GetElement())->GetPtr()){
    ++free_bloc_it;
  }

  free_blocks_.InsterBefore((DoubleLinkElement*)memory_block, free_bloc_it.GetElement());
  MemoryBlock *forward_block = (MemoryBlock*)free_bloc_it.GetElement();
  if (!free_bloc_it.IsLast() && AreBlockContinuous(memory_block, forward_block)){
    memory_block->Resize(memory_block->GetSize() + forward_block->GetSize());
    free_blocks_.Remove((DoubleLinkElement*)forward_block);
    unused_blocks_.PushBack((DoubleLinkElement*)forward_block);
  }
  free_bloc_it.SetPositionByElement((DoubleLinkElement*)memory_block);
  --free_bloc_it;
  MemoryBlock * previous_block = (MemoryBlock*)free_bloc_it.GetElement();
  if (!free_bloc_it.IsFirst() && AreBlockContinuous(previous_block, memory_block)){
    previous_block->Resize(previous_block->GetSize() + memory_block->GetSize());
    free_blocks_.Remove((DoubleLinkElement*)memory_block);
    unused_blocks_.PushBack((DoubleLinkElement*)memory_block);
  }


}
///Get total memory managed
int MemoryManager::GetManagedMemorySize()const{
  return managed_size_;
}
///Get totl allocated memory
int MemoryManager::GetAllocatedMemorySize()const{
  return allocated_size_;
}
//Check coherence
void MemoryManager::CheckCoherence()const{
  DoubleLinkIterator free_block_it = free_blocks_.GetIterator();
  unsigned char previus_ptr = 0;
  while (!free_block_it.IsLast()){
    int current_ptr = (int)((MemoryBlock*)free_block_it.GetElement())->GetPtr();
    assert(current_ptr > previus_ptr && "Memory fault");
    previus_ptr = current_ptr;
    ++free_block_it;
  }
  //Check duplicated pointers
  DoubleLinkIterator  used_block_it_a = used_blocks_.GetIterator();
  while (!used_block_it_a.IsLast()){
    DoubleLinkIterator used_block_it_b = used_blocks_.GetIterator();
    used_block_it_b.SetPositionByElement(used_block_it_a.GetElement());
    ++used_block_it_b;
    void* ptr_a = ((MemoryBlock*)used_block_it_a.GetElement())->GetPtr();
    while (!used_block_it_b.IsLast()){
      void *ptr_b = ((MemoryBlock*)used_block_it_b.GetElement())->GetPtr();
       assert(ptr_a != ptr_b && "Duplicated pointer in used blcks, memory leak found");
      ++used_block_it_b;
    }
    ++used_block_it_a;
  }

}

MemoryManager* MemoryManager::Get(){
  if (ptr_ == nullptr){
    ptr_ = new MemoryManager();
  }
  return ptr_;
}
