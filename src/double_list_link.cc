#include "doube_link_list.h"
#include <iostream>

DoubleLinkList::DoubleLinkList(){
  first_ = &first_element_;
  last_ = &last_element_;
  first_->previus_ = 0;
  first_->next_ = 0;
  Clear();
}
DoubleLinkList::~DoubleLinkList()
{
  Clear();
}
void DoubleLinkList::Inster(DoubleLinkElement * element, int index){
  assert( (index > 0 || index < size_)  && "Index is higher than size of list" );
  DoubleLinkElement * next = first_;
  for (int  i = 0; i < index; i++){
    next = next->next_;
  }
  InsterAfter(element, next);
}

void DoubleLinkList::InsterAfter(DoubleLinkElement * element, DoubleLinkElement *after_element){

  //Save next element of the selected element to inster
  DoubleLinkElement * next = after_element->next_;
 //Next
  element->next_ = next;
  next->previus_ = element;
  //Previus
  after_element->next_ = element;
  element->previus_ = after_element;
  ++size_;
}

void DoubleLinkList::InsterBefore(DoubleLinkElement * element, DoubleLinkElement *before_element){
  assert(!Exist(element) && "Element is already on the array" );
  before_element->previus_->next_ = element;
  element->previus_ = before_element->previus_;
  element->next_ = before_element;
  before_element->previus_ = element;
  ++size_;
}

DoubleLinkElement* DoubleLinkList::PopBack(){
  if (size_ == 0){
    return nullptr;
  }
  DoubleLinkElement *element = last_->previus_;
  Remove(element);
  return element;
}
///Pop elemtn to the top of list
DoubleLinkElement*  DoubleLinkList::PopFront(){
  if (size_ == 0){
    return nullptr;
  }
  DoubleLinkElement *element = first_->next_;
  Remove(element);
  return element;
}

void DoubleLinkList::Replace(DoubleLinkElement * new_element, DoubleLinkElement * old_element){
  new_element = old_element;
  old_element->Unlink();

}

//DoubleLinkElement& operator [](size_t idx);

int DoubleLinkList::GetElementIndex(const DoubleLinkElement *element)const{
  DoubleLinkElement *next = first_->next_;
  int i = 0;
  while (next != last_){
    if (next == element){
      return i;
    }
    ++i;
     next = next->next_;
  }
  return -1;
}

DoubleLinkElement* DoubleLinkList::Get(int index){
  assert( index < size_  && "Element outside of the list" );
  DoubleLinkElement *next = first_->next_;
  for (int i = 0; i < index; i++){
    next = next->next_;
  }
  return next;
}

void DoubleLinkList::Remove(DoubleLinkElement * element){
  assert(element && "Element doesnt exists");
  DoubleLinkElement * previus = element->previus_;
  DoubleLinkElement * next = element->next_;

  previus->next_ = next;
  next->previus_ = previus;

  element->Unlink();
  size_--;
}

void DoubleLinkList::Clear(){
  size_ = 0;
  first_->next_ = last_;
  last_->previus_ = first_;
}
