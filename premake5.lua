-- Create Solution

solution "Memory Lib"

configurations{"Debug","Release"}
location("debug")
targetdir("debug/bin")
debugdir("data")


project "MemoryManager"
	kind "ConsoleApp"
	language "C++"
	libdirs{}
	includedirs{"include","deps"}
	files{
		"include/**.h",
		"include/**.c",
		"include/**.cc",
		"include/**.inl",

		"deps/**.h",
		"deps/**.c",
		"deps/**.cc",
		"deps/**.inl",
		
		"src/**.cc",
		"src/**.c",
		"src/**.cpp",
	}
	configuration "Debug"


